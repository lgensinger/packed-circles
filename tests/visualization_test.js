import test from "ava";

import { configuration, configurationLayout } from "../src/configuration.js";
import { PackedCircles } from "../src/index.js";

/******************** EMPTY VARIABLES ********************/

// initialize
/*let pc = new PackedCircles();

// TEST INIT //
test("init", t => {

    t.true(pc.height === configurationLayout.height);
    t.true(pc.width === configurationLayout.width);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    pc.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == configurationLayout.height);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == configurationLayout.width);

});

/******************** DECLARED PARAMS ********************/

let testWidth = 300;
let testHeight = 500;
let testData = [
    { path: "ABC.DEF", value: 5 },
    { path: "ABC", value: 0 },
    { path: "ABC.GHI", value: 1 },
    { path: "ABC.GHI.JKL", value: 8 },
    { path: "ABC.GHI.MNO", value: 7 },
    { path: "ABC.DEF.OKD", value: 10 },
    { path: "ABC.BLA", value: 3 },
    { path: "ABC.DEF.KIP", value: 4 }
];

// initialize
let pcp = new PackedCircles(testData, testWidth, testHeight);

// TEST INIT //
test("init_params", t => {

    t.true(pcp.height === testHeight);
    t.true(pcp.width === testWidth);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render_params", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    pcp.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == testHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == testWidth);

});*/
