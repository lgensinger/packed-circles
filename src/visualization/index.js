import { ChartLabel, PackLayout as PL, RadialGrid, simplifyLargeNumber } from "@lgv/visualization-chart";
import { pack, stratify } from "d3-hierarchy";
import { interpolateString } from "d3-interpolate";
import { select } from "d3-selection";
import { transition } from "d3-transition";

import { configuration, configurationData, configurationLayout } from "../configuration.js";

/**
 * PackedCircles is a hierarchy visualization.
 * @param {array} data - objects where each represents a series in the collection
 * @param {integer} height - artboard height
 * @param {Class} PackLayout - javascript class representing the layout abstraction
 * @param {various} padding - float or function to set padding between bubbles
 * @param {integer} width - artboard width
 */
class PackedCircles extends RadialGrid {

    constructor(data, width=configurationLayout.width, height=configurationLayout.height, PackLayout=null, padding=configurationLayout.padding, delimiter=configurationData.delimiter, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, width, height, PackLayout ? PackLayout : new PL(data,width,height,padding,delimiter), label, name);

        // update self
        this.classLabel = `${label}-label`;
        this.classLabelPartial = `${label}-label-partial`;
        this.classNode = `${label}-node`;
        this.label = null;
        this.labelPartial = null;
        this.node = null;

    }

    /**
     * Declare circle mouse events.
     */
    configureCircleEvents() {
        this.node
            .on("click", (e,d) => this.configureEvent("node-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classNode} active`);

                // send event to parent
                this.configureEvent("node-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classNode);

                // send event to parent
                this.artboard.dispatch("node-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style labels in SVG dom element.
     */
    configureLabels() {
        this.label
            .attr("class", this.classLabel)
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("data-depth", d => d.depth)
            .attr("data-children", d => d.children ? true : false)
            .attr("text-anchor", "middle")
            .attr("pointer-events", "none")
            .attr("visibility", d => (this.Label.determineHeight(this.Data.extractLabel(d)) * 2 > d.r * 2 || d.children) ? "hidden" : "visible")
            .attr("y", d => d.y - this.radius);
    }

    /**
     * Position and minimally style label partials in SVG dom element.
     */
    configureLabelPartials() {
        this.labelPartial
            .transition().duration(1000)
            .attr("class", this.classLabelPartial)
            .attr("x", d => d.x - this.radius)
            .attr("dy", (d,i) => this.Label.calculateDy(i, d, d.r * 2))
            .textTween((d,i) => this.tweenText(this.Data.extractLabel(d), this.Data.extractValue(d), d.r * 2, i));
    }

    /**
     * Position and minimally style nodes in SVG dom element.
     */
    configureNodes() {
        this.node
            .transition().duration(1000)
            .attr("class", this.classNode)
            .attr("data-id", d => this.Data.extractId(d))
            .attr("data-full-path", d => d.data.path)
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("data-depth", d => d.depth)
            .attr("data-children", d => d.children ? true : false)
            .attr("fill", "lightgrey")
            .attr("opacity", 0.4)
            .attr("cx", d => d.x - this.radius)
            .attr("cy", d => d.y - this.radius)
            .attr("r", d => d.r);
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate circles
        this.node = this.generateNodes(this.content);
        this.configureNodes();
        this.configureCircleEvents();

        // generate text labels
        this.label = this.generateLabels(this.content);
        this.configureLabels();

        // generate label partials so they stack
        this.labelPartial = this.generateLabelPartials(this.label);
        this.configureLabelPartials();

    }

    /**
     * Generate label partials in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateLabelPartials(domNode) {
        return domNode
            .selectAll(`.${this.classLabelPartial}`)
            .data(d => [d, d])
            .join(
                enter => enter.append("tspan"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate labels in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateLabels(domNode) {
        return domNode
            .selectAll(`.${this.classLabel}`)
            .data(this.data.descendants())
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct bubble group in HTML DOM.
     * @param {selection} domNode - d3 selection
     * @returns A d3.js selection.
     */
    generateNodes(domNode) {
        return domNode
            .selectAll(`.${this.classNode}`)
            .data(this.data.descendants())
            .join(
                enter => enter.append("circle"),
                update => update,
                exit => exit.transition()
                    .duration(1000)
                    .attr("cx", 0)
                    .attr("cy", 0)
                    .attr("transform", "scale(0)")
                    .remove()
            );
    }

};

export { PackedCircles };
export default PackedCircles;
