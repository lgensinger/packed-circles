import { configuration as config } from "@lgv/visualization-chart";
import packagejson from "../package.json";

const configuration = {
    branding: process.env.LGV_BRANDING || config.branding,
    name: packagejson.name.replace("/", "-").slice(1)
};

const configurationData = {
    delimiter: process.env.LGV_PARSE_DELIMITER || "."
}

const configurationLayout = {
    height: process.env.LGV_HEIGHT || 600,
    padding: d => d.children ? 100 : 1,
    width: process.env.LGV_WIDTH || 600
}

export { configuration, configurationData, configurationLayout };
export default configuration;
