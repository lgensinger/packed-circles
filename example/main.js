import packageJson from "../package.json";
import { PackedCircles } from "../src/index.js";
import { processEvent, renderDefault } from "@lgv/visualization-chart";

let data = [
    { path: "x" },
    { path: "x.a" },
    { path: "x.y" },
    { path: "x.z" },
    { path: "x.z.b" },
    { path: "x.z.c" },
    { path: "x.y.d" },
    { path: "x.y.e" },
    { path: "x.y.f" }
];

// get elements
let container = document.getElementsByTagName("figure")[0];
let sourceContainer = document.getElementsByTagName("code")[0];
let outputContainer = document.getElementsByTagName("code")[1];

/**
 * Render initial visualization.
 * @param {element} container - DOM element
 * @param {array} data - object with key/value pairs of path
 */
function startup(data,container) {

    // update version in header
    let h1 = document.querySelector("h1");
    let title = h1.innerText;
    h1.innerHTML = `${title} <span>v${packageJson.version}</span>`;

    // render source data
    renderDefault(data,sourceContainer,outputContainer);

    // determine width
    let width = container.offsetWidth > 500 ? 500 : container.offsetWidth;

    // initialize
    const pc = new PackedCircles(data,width,width,null,10);

    // render visualization
    pc.render(container);

}

// load document
document.onload = startup(data,container);

// attach events
container.outputContainer = outputContainer;
container.addEventListener("node-click", processEvent);
container.addEventListener("node-mouseover", processEvent);
container.addEventListener("node-mouseout", processEvent);