# Packed Circles

ES6 d3.js packed circles hierarchy visualization. 

## Install

```bash
# install package
npm install @lgv/packed-circles
```

## Data Format

The following values are the expected input data structure; the paths will be used as the item's id.

```json
[
    {
        "path": "some"
    },
    {
        "path": "some.path"
    },
    {
        "path": "some.path.other"
    },
    {
        "path": "some.another"
    }
]
```

## Use Module

```bash
import { PackedCircles } from "@lgv/packed-circles";

// have some data
let data = [
    {
        "path": "some"
    },
    {
        "path": "some.path"
    },
    {
        "path": "some.path.other"
    },
    {
        "path": "some.another"
    }
];

// initialize
const pc = new PackedCircles(data);

// render visualization
pc.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_PARSE_DELIMITER` | string | single character to parse full path strings |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |
| node circle | `node-click` | on click |
| node circle | `node-mouseover` | on hover |
| node circle | `node-mousemout` | on un-hover |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-packed-circles` | top-level svg element |
| `lgv-content` | content inside artboard inside padding |
| `lgv-node` | node circle |
| `lgv-label` | node text label group |
| `lgv-label-partial` | node text tspan for label/value |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd packed-circles

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const pc = new PackedCircles(data);
# add pc.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
